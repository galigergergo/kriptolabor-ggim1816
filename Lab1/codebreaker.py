from crypto import codebreak_vigenere

# read words from file
with open('words.txt', 'r') as file:
    words = file.read()
possible_keys = [word.upper() for word in words.split()]

# read message to be decoded
with open('not_a_secret_message.txt', 'r') as file:
    for last_line in file:
        pass

decrypted, key = codebreak_vigenere(last_line, possible_keys)
with open('not_a_secret_message_decrypted.txt', 'w') as file:
    words = file.write('\nKey: {}\n{}\n'.format(key, decrypted))
