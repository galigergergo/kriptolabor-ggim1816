Name: Galiger Gergő, ggim1816

In 1-3 sentences per section, comment on your approach to each of the parts of the assignment. What was your high-level strategy? How did you translate that into code? Did you make use of any Pythonic practices? We want you to reflect on your coding style, and how you are maximally leveraging Python's utilities.

# Caesar Cipher
Using the ascii_uppercase string, I managed to find the corresponding letters by
a simple periodic shifting of it by 3.
I used string concatenation and list comprehension to complete the task.

# Vigenere Cipher
Using the ascii_uppercase string, I managed to find the corresponding letters by
a simple periodic shifting of it by the value of the keyword letters.
I used string concatenation and list comprehension to complete the task.

# Scytale Cipher
I managed to find a way to calulate the scytale encryption correscponding index
for the letter indeces.
By using list comprehension, the code got really compact and easy to understand.

# Railfence Cipher
While enrpyting, I built up num_rails number of strings by imaginarily looping over
the length of the text in a V shape, which I then concatenated to get the encrypted
text. For decryption I physicaly built up a list of the corresponding letter indeces,
which I then used to find the original order of the letters.
I used string concatenation and many list comprehensions to complete the task.

# Merkle-Hellman Knapsack Cryptosystem

