"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: Galiger Gergő - ggim1816

Replace this placeholder text with a description of this module.
"""
import string
from bisect import bisect_left

#################
# CAESAR CIPHER #
#################

def encrypt_caesar(plaintext):
    """Encrypt a plaintext using a Caesar cipher.

    Encrypt the plaintext in a single loop on its length by finding the
    encrypted letter as a periodic shifting of the ascii_uppercase string.

    :param plaintext: The message to encrypt.
    :type plaintext: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.
    return "".join([string.ascii_uppercase[(string.ascii_uppercase.index(plaintext[i]) + 3) % 26]
        for i in range(len(plaintext))])


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    Encrypt the plaintext in a single loop on its length by finding the
    encrypted letter as a periodic shifting of the ascii_uppercase string.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.
    return "".join([string.ascii_uppercase[(string.ascii_uppercase.index(ciphertext[i]) + 23) % 26]
        for i in range(len(ciphertext))])


###################
# VIGENERE CIPHER #
###################

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Encrypt the plaintext in a single loop on its length by finding the
    encrypted letter as a periodic shifting of the ascii_uppercase string
    with the coresponding keyword letter.

    :param plaintext: The message to encrypt.
    :type plaintext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.
    return "".join([string.ascii_uppercase[(string.ascii_uppercase.index(plaintext[i])
        + string.ascii_uppercase.index(keyword[i % len(keyword)])) % 26]
        for i in range(len(plaintext))])


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Decrypt the ciphertext in a single loop on its length by finding the
    decrypted letter as a periodic shifting of the ascii_uppercase string
    with the coresponding keyword letter.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.
    return "".join([string.ascii_uppercase[(string.ascii_uppercase.index(ciphertext[i])
        + 26 - string.ascii_uppercase.index(keyword[i % len(keyword)])) % 26]
        for i in range(len(ciphertext))])


###################
# SCYTALE CIPHER  #
###################

def encrypt_scytale(plaintext, circumference):
    """Encrypt plaintext using a Scytale cipher with a keyword.

    Encrypt the plaintext in a single loop on its length by calculating
    the index of each encrypted letter with the formula below.

    :param plaintext: The message to encrypt.
    :type plaintext: str
    :param circumference: The circumference of the table used in the
        Scytale cipher.
    :type circumference: str

    :returns: The encrypted ciphertext.
    """
    # number of letters in a row
    lettersInARow = len(plaintext) // circumference
    # number of letters remaining outside the rectangle
    remainingLetters = len(plaintext) % circumference
    # at which letter of the message does the regular rectangle start
    borderIndex = ((lettersInARow + 1) * (remainingLetters))
    return "".join(plaintext[i % (lettersInARow + (1 if i < borderIndex else 0))
        * circumference + i // ((lettersInARow) + (1 if i < borderIndex else 0))]
        for i in range(len(plaintext)))


def decrypt_scytale(ciphertext, circumference):
    """Decrypt ciphertext using a Scytale cipher with a keyword.

    Decrypt the ciphertext in a single loop on its length by calculating
    the index of each decrypted letter with the formula below.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str
    :param circumference: The circumference of the table used in the
        Scytale cipher.
    :type circumference: str

    :returns: The decrypted plaintext.
    """
    # number of letters in a row
    lettersInARow = len(ciphertext) // circumference
    # number of letters remaining outside the rectangle
    remainingLetters = len(ciphertext) % circumference
    # at which letter of the message does the regular rectangle start
    borderIndex = ((lettersInARow + 1) * (remainingLetters))
    return "".join(ciphertext[i % (circumference + (1 if i < borderIndex else 0))
        * lettersInARow + i // ((circumference) + (1 if i < borderIndex else 0))]
        for i in range(len(ciphertext)))


####################
# RAILFENCE CIPHER #
####################

def encrypt_railfence(plaintext, num_rails):
    """Encrypt plaintext using a Railfence cipher with a keyword.

    Builds the encrypred text by concatenating num_rails number of strings.
    These strings start as empty strings and get filled with plaintext letters
    in a repeating V shape of the railfence grid.

    :param plaintext: The message to encrypt.
    :type plaintext: str
    :param num_rails: The number of rails used in the Railfence cipher.
    :type num_rails: int

    :returns: The encrypted ciphertext.
    """
    period = 2 * (num_rails - 1)
    rail = [""] * num_rails
    for i in range(len(plaintext)):
        rail[i % period if i % period < num_rails else num_rails - i % period % num_rails - 2] += plaintext[i]
    return "".join(rail)


# build the grid of the railfence cipher as a 2D list of letter indeces
# and return the list of indeces in the ciphered order
def buildGrid(indexList, numr_ails):
    grid = [[None] * len(indexList) for n in range(numr_ails)]
    period = list(range(numr_ails - 1)) + list(range(numr_ails - 1, 0, -1))
    for n, x in enumerate(indexList):
        grid[period[n % len(period)]][n] = x
    return [c for rail in grid for c in rail if c is not None]


def decrypt_railfence(ciphertext, num_rails):
    """Decrypt ciphertext using a Railfence cipher with a keyword.

    Get the list of indeces in ciphered order usign buildGrid function
    and use this to place the letters to their original position.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str
    :param num_rails: The number of rails used in the Railfence cipher.
    :type num_rails: int

    :returns: The decrypted plaintext.
    """
    indexList = range(len(ciphertext))
    cipheredIndeces = buildGrid(indexList, num_rails)
    return ''.join(ciphertext[cipheredIndeces.index(i)] for i in indexList)


###########################
# INTELLIGENT CODEBREAKER #
###########################

# delete punctuation marks and spaces + save their location
# for later reconstruction
def processText(text):
    return ''.join(letter for letter in text if letter.isalpha()),\
        [(i, text[i]) for i in range(len(text)) if not text[i].isalpha()]


# reconstruct the processed text using the saved punctuation locations
# mode - 0 -> reconstruct using whole punctuation list
# mode - 1 -> reconstruct using only spaces and '-s
def reconstructText(text, punctuation, mode=0):
    for entry in punctuation:
        text = text[:entry[0]] + entry[1] + text[entry[0]:]
    if mode == 1:
        return ''.join(letter for letter in text if letter.isalpha() or letter == ' ' or letter == '\'')
    return text


# check percentage of english words in the text
def checkForEnglish(text, wordList):
    count = 0
    for word in text:
        i = bisect_left(wordList, word)
        if i != len(wordList) and wordList[i] == word:
            count += 1
    return count / len(text)


# decode a text encoded using vigerere knowing a list of possible keys
def codebreak_vigenere(ciphertext, possible_keys):
    processedText, punctuation = processText(ciphertext)
    dictionary = possible_keys
    possible_keys = [key for key in possible_keys if key.isalpha()]

    maxPercentage = 0
    bestKey = ''
    for key in possible_keys:
        wordList = reconstructText(decrypt_vigenere(processedText, key), punctuation, 1).split()
        percentage = checkForEnglish(wordList, dictionary)
        if percentage > maxPercentage:
            maxPercentage = percentage
            bestKey = key
            print('\nKey: {}\n{}\n'.format(key, reconstructText(decrypt_vigenere(processedText, key), punctuation)))

    return reconstructText(decrypt_vigenere(processedText, bestKey), punctuation), bestKey


########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################

def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    You'll also need to use the random module's `randint` function, which you
    will have to import.

    Somehow, you'll have to return all three of these values from this function!
    Can we do that in Python?!

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    # Your implementation here.
    raise NotImplementedError('generate_private_key is not yet implemented!')


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in
    the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one or two lines using list comprehensions.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-tuple public key
    """
    # Your implementation here.
    raise NotImplementedError('create_public_key is not yet implemented!')


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte. In principle, we should work for any value of n, but we'll
        assert that it's fine to operate byte-by-byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """
    # Your implementation here.
    raise NotImplementedError('encrypt_mh is not yet implemented!')


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """
    # Your implementation here.
    raise NotImplementedError('decrypt_mh is not yet implemented!')
