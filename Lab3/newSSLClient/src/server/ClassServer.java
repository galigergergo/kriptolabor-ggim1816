package server;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.security.KeyStore;
import javax.net.ssl.*;

public class ClassServer implements Runnable {

    private ServerSocket server;

    public static final Integer PORT = 443;

    protected ClassServer() {

        try {
            // set up key manager to do server authentication
            SSLContext ctx;
            KeyManagerFactory kmf;
            KeyStore ks;
            char[] passphrase = "changeit".toCharArray();

            ctx = SSLContext.getInstance("SSL");
            kmf = KeyManagerFactory.getInstance("SunX509");
            ks = KeyStore.getInstance("JKS");

            ks.load(new FileInputStream("cacerts"), passphrase);
            kmf.init(ks, passphrase);
            ctx.init(kmf.getKeyManagers(), null, null);

            // initialize SSL server socket
            server = ctx.getServerSocketFactory().createServerSocket(PORT);
            // start new server thread
            (new Thread(this)).start();
            System.out.println("Server started...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run()
    {
        SSLSocket socket;

        try {
            socket = (SSLSocket) server.accept();
        } catch (IOException e) {
            System.out.println("Server error: " + e.getMessage());
            e.printStackTrace();
            return;
        }

        System.out.println("Request received...");
        (new Thread(this)).start();

        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            try {
                File file = new File("banca.html");
                byte[] bytecodes = Files.readAllBytes(file.toPath());
                out.write("HTTP/1.0 200 OK");
                out.newLine();
                out.write("Content-Type: text/html");
                out.newLine();
                out.newLine();
                out.write("<html><body><h1>Https Server Works</h1></body></html>");
                out.newLine();
                out.flush();
            } catch (Exception e) {
                e.printStackTrace();
                out.write("HTTP/1.0 400 " + e.getMessage() + "\r\n");
                out.write("Content-Type: text/html\r\n\r\n");
                out.flush();
            }
        } catch (IOException ex) {
            System.out.println("error writing response: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}