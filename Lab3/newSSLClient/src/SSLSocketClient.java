import javax.net.ssl.*;
import java.io.*;
import java.security.KeyStore;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.sql.SQLOutput;

public class SSLSocketClient {

    public static void main(String[] args) throws Exception {
        try {
            SSLSocketFactory factory = (SSLSocketFactory)SSLSocketFactory.getDefault();
            SSLSocket socket = (SSLSocket)factory.createSocket("bnr.ro", 443);
//            SSLSocket socket = (SSLSocket)factory.createSocket("localhost", 443);

            socket.addHandshakeCompletedListener(hce -> {
                X509Certificate cert;
                try {
                    cert = (X509Certificate)hce.getPeerCertificates()[0];
                    System.out.println("SSL Certificate information:");
                    System.out.println(" * Version: " + cert.getVersion());
                    System.out.println(" * Serial number: " + cert.getSerialNumber());
                    String issuerName = cert.getIssuerX500Principal().getName().split(",")[0].substring(3);
                    System.out.println(" * Issuer name: " + issuerName);
                    System.out.println(" * Issue date: " + cert.getNotBefore());
                    System.out.println(" * Expiration date: " + cert.getNotAfter());
                    String orgName = cert.getSubjectX500Principal().getName().split(",")[2].substring(2);
                    System.out.println(" * Organization name: " + orgName);
                    System.out.println(" * Signature algorithm: " + cert.getSigAlgName());
                    System.out.println(" * Public key: " + cert.getPublicKey());
                } catch (SSLPeerUnverifiedException e) {
                    e.printStackTrace();
                }
            });

            socket.startHandshake();

            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));

            out.println("GET /Home.aspx HTTP/1.0");
            out.println();
            out.flush();

            if (out.checkError()) {
                System.out.println("SSLSocketClient:  java.io.PrintWriter error");
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            PrintWriter printWriter = new PrintWriter("banca.html", "UTF-8");

            Boolean isHtml = false;
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.startsWith("<!DOCTYPE HTML")) {
                    isHtml = true;
                }
                if (isHtml) {
                    printWriter.write(inputLine + "\n");
                }
            }

            printWriter.close();
            in.close();
            out.close();
            socket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}