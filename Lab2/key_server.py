import socket
from threading import Thread
from json import loads


# server host/ip address and port number
HOST = 'keyserver'
PORT = 65000

# dictionary of registered clients
clients = {}


# handler for processing one client request
def rec_thread(conn, addr, clients):
    with conn:
        print('THREAD -', addr, '- connected')

        # get message from socket
        request = conn.recv(1024).decode('utf-8')

        # convert request to dictionary
        dict_req = loads(request)

        # process request
        if 'public_key' in dict_req.keys():
            # create or update client info
            if 'client_id' in dict_req.keys():
                clients[dict_req['client_id']] = dict_req['public_key']
                conn.sendall('updated'.encode())
                print('THREAD -', addr, '- client', dict_req['client_id'], 'updated')
            else:
                conn.sendall('error'.encode())
                print('THREAD -', addr, '- error in updating client', dict_req['client_id'])
        else:
            # send requested client public key
            if 'client_id' in dict_req.keys() and dict_req['client_id'] in clients.keys():
                conn.sendall(str(clients[dict_req['client_id']]).encode())
                print('THREAD -', addr, '- client', dict_req['client_id'], 'public key sent')
            else:
                conn.sendall('error'.encode())
                print('THREAD -', addr, '- error in finding public key for client', dict_req['client_id'])


# start the server
def run():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        # bind socket
        s.bind((HOST, PORT))
        s.listen()
        print('KeyServer listening on', HOST + ':' + str(PORT))

        while True:
            # wait for requests
            conn, addr = s.accept()

            # start request handler thread
            t = Thread(target=rec_thread, args=(conn, addr, clients, ))
            t.start()


run()
