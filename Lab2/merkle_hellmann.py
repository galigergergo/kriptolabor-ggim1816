from utils import modinv, coprime, byte_to_bits, bits_to_byte
from random import randint
from itertools import chain


def generate_private_key(n=8):
    """Generate a private key to use with the
                Merkle-Hellman Knapsack Cryptosystem.

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`,
    with `w` a n-tuple, and q and r ints.
    """

    # superincreasing sequence
    w = [randint(2, 10)]
    for i in range(n - 1):
        w.append(randint(sum(w) + 1, 2 * sum(w)))

    # choose q greater than the sum of w
    q = randint(sum(w) + 1, 2 * sum(w))

    # find r between 2 and q that is coprime with q
    r = randint(2, q)
    while not coprime(r, q):
        r = randint(2, q)

    return w, q, r


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-tuple public key
    """

    # return the public key using beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q
    return [private_key[2] * private_key[0][i] % private_key[1]
                                    for i in range(len(private_key[0]))]


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """

    # separate message into array of bits
    msg_bits = list(chain.from_iterable([byte_to_bits(b) for b in bytes(message, 'utf-8')]))

    # complete bit array with 0s for its length to be divisible by n
    n = len(public_key)
    if len(msg_bits) % n > 0:
        msg_bits.extend([0 for i in range(n - len(msg_bits) % n)])

    # return encrypted message using: c = sum of a_i * b_i for i = 1 to n
    return [sum([msg_bits[n * j + i] * public_key[i] for i in range(n)])
                                        for j in range(len(msg_bits) // n)]


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: str of decrypted characters
    """

    w, q, r = private_key
    n = len(w)

    # compute modular inverse of r mod q using the Extended Euclidean algorithm
    s = modinv(r, q)

    # for each message number compute c' = cs (mod q)
    dec_msg = [c * s % q for c in message]

    # solve the superincreasing subset sum problem for all numbers
    dec_bits = []
    for c in dec_msg:
        bits = []
        for i in range(n):
            if c < w[n - i - 1]:
                bits.append(0)
            else:
                bits.append(1)
                c -= w[n - i - 1]
        bits.reverse()
        dec_bits.extend(bits)

    # remove any added 0s from the end of the bit array
    dec_bits = dec_bits[:len(dec_bits) - len(dec_bits) % 8]

    # remove 0 bytes from the end of bit array
    dec_bits = list(chain.from_iterable([dec_bits[i * 8: (i + 1) * 8]
                                    for i in range(len(dec_bits) // 8)
                                    if sum(dec_bits[i * 8: (i + 1) * 8]) > 0]))

    # return the bit array converted back to string
    return "".join([chr(bits_to_byte(dec_bits[i * 8: (i + 1) * 8]))
                                    for i in range(len(dec_bits) // 8)])
