import socket
from json import loads
from merkle_hellmann import decrypt_mh
from clients import prepare_communication
from solitaire import encrypt_solitaire, decrypt_solitaire


# own host/ip address and port number
MYHOST = 'client2'
MYPORT = 65002

# other client host/ip address and port number
OTHERHOST = 'client1'
OTHERPORT = 65001

# message to be sent to other client
TOSEND = 'In computer science, cryptography refers to secure information and communication techniques derived from mathematical concepts and a set of rule-based calculations called algorithms, to transform messages in ways that are hard to decipher. These deterministic algorithms are used for cryptographic key generation, digital signing, verification to protect data privacy, web browsing on the internet, and confidential communications such as credit card transactions and email.'


# send a message to a socket address
def send(host, port, message):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        s.sendall(message.encode())


# get keys
my_pri_key, my_pub_key, other_pub_key = prepare_communication(MYPORT, OTHERPORT)

# wait for other client message
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((MYHOST, MYPORT))
    s.listen()
    conn, addr = s.accept()
    message = conn.recv(4096).decode()

    # decrypt the message with own mh private key to get the solitaire deck
    solitaire_key = loads(decrypt_mh(loads(message), my_pri_key))
    print('Solitaire deck received and decrypted with mh:', solitaire_key)

    message = ''
    while message != 'exit':
        # get user input message
        TOSEND = input('Send a message: ')

        # send an encrypted message using the common solitaire deck
        send(OTHERHOST, OTHERPORT, encrypt_solitaire(TOSEND, solitaire_key))
        print('Message encrypted with solitaire and sent:', TOSEND)

        # break if you exited
        if TOSEND == 'exit':
            break

        # get and decrypt message from other client
        conn, addr = s.accept()
        message = conn.recv(4096).decode()
        message = decrypt_solitaire(message, solitaire_key)
        print('Message received:', message)
