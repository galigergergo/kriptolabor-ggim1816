import socket


# server host/ip address and port number
HOST = 'keyserver'
PORT = 65000

# requests to be sent for testing the server
requests = [
    '{ "client_id": 1 }',
    '{ "client_id": 1, "public_key": "123" }',
    '{ "client_id": 2, "public_key": "234" }',
    '{ "client_id": 1 }',
    '{ "client_id": 2 }',
    '{ "client_id": 3 }',
    '{ "client_id": 2, "public_key": "333" }',
    '{ "client_id": 2 }',
]

# expected responses from server for the test requests
expected = [
    'error',
    'updated',
    'updated',
    '123',
    '234',
    'error',
    'updated',
    '333'
]


# send one request to the server and return the decoded response
def simulate_request(req):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))

        # send request
        s.sendall(req.encode())

        # return decoded response
        return s.recv(1024).decode()


# number of successful test requests
success = 0

# test the server for all requests
for i in range(len(requests)):
    if simulate_request(requests[i]) == expected[i]:
        success += 1
    else:
        break

# print test summary
if success == len(requests):
    print('Test completed successfuly.')
else:
    print('Test failed:', requests[success], expected[success])
