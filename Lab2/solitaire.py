def move_down(deck, card, move):
    """Move a card down a number of cards in the deck.

    :param deck: The order of the deck of cards.
    :type deck: list of ints
    :param card: The value of card to move.
    :type card: int
    :param move: The number of move steps.
    :type move: int

    """

    # find new index
    new_index = deck.index(card) + move

    # manage circular move
    if new_index >= len(deck):
        new_index -= len(deck) - 1

    # move the card in the deck
    deck.remove(card)
    deck.insert(new_index, card)


def triple_cut(deck):
    """Perform a triple cut on the deck.
        (swap cards above the highest joker with the cards below the lowest)

    :param deck: The order of the deck of cards.
    :type deck: list of ints

    """

    # find joker indeces
    index_a = deck.index(52)
    index_b = deck.index(53)

    # check their order
    top_index, bot_index = index_b, index_a
    if index_b > index_a:
        top_index, bot_index = index_a, index_b

    # perform the triple cut
    deck.extend(deck[top_index:bot_index + 1])
    deck.extend(deck[:top_index])
    del(deck[:bot_index + 1])


def count_cut(deck):
    """ Perform a count cut on the deck.
        (moves the top {bottom card value} cards to just above the bottom card)

    :param deck: The order of the deck of cards.
    :type deck: list of ints

    """

    # get value of bottom card
    count = deck[-1]

    # both jokers are 52
    if count == 53:
        count = 52

    # perform the coutn cut
    deck[-1:1] = deck[:count + 1]
    del(deck[:count + 1])


def step(deck):
    """ Find the key by performing a solitaire cypher step on the deck.
        (move jokers -> triple cut -> count cut -> find key)

    :param deck: The order of the deck of cards.
    :type deck: list of ints

    :returns: generated key as int
    """

    # move the jokers down
    move_down(deck, 52, 1)
    move_down(deck, 53, 2)

    # perform the triple cut
    triple_cut(deck)

    # perform the count cut
    count_cut(deck)

    # find the key by using the value of the top card
    key = deck[deck[0] if deck[0] == 53 else deck[0] + 1]

    # if the key is a joker, repeat the algorithm
    if key in (52, 53):
        return step(deck)

    # return the key
    return key


def encrypt_solitaire(message, deck):
    """Encrypt an outgoing message using a deck configuration.

    :param message: The message to be encrypted.
    :type message: string
    :param deck: The order of the deck of cards.
    :type deck: list of ints

    :returns: Encrypted message as a string.
    """

    # keep only letters in the message
    message = ''.join(filter(str.isalpha, message))

    # list of ascii letter values
    letters = list(range(ord('A'), ord('Z') + 1)) +\
                list(range(ord('a'), ord('z') + 1))

    enc_msg = ''
    for c in message:
        # shuffle and generate key
        key = step(deck)

        # get the encrypted value of the letter
        enc_msg += chr(letters[(letters.index(ord(c)) + key) % 52])

    # return the encrypted message
    return enc_msg


def decrypt_solitaire(message, deck):
    """Decrypt an incoming message using the encryption deck configuration.

    :param message: The message to be decrypted.
    :type message: string
    :param deck: The order of the deck of cards.
    :type deck: list of ints

    :returns: Decrypted message as a string.
    """

    # list of ascii letter values
    letters = list(range(ord('A'), ord('Z') + 1)) +\
                list(range(ord('a'), ord('z') + 1))

    dec_msg = ''
    for c in message:
        # shuffle and generate key
        key = step(deck)

        # get the decrypted value of the letter
        dec_msg += chr(letters[(letters.index(ord(c)) - key + 52) % 52])

    # return the decrypted message
    return dec_msg
