from solitaire import encrypt_solitaire, decrypt_solitaire
from random import shuffle

# number of random deck shuffles
n_test = 5

# messages to be tested
msg_test = [
    "szia",
    "Ez egy mondat",
    "The Merkle-Hellman knapsack cryptosystem was one of the earliest public key cryptosystems. It was published by Ralph Merkle and Martin Hellman in 1978. A polynomial time attack was published by Adi Shamir in 1984. As a result, the cryptosystem is now considered insecure.",
    "Merkle-Hellman is a public key cryptosystem, meaning that two keys are used, a public key for encryption and a private key for decryption. It is based on the subset sum problem (a special case of the knapsack problem).[5] The problem is as follows: given a set of integers A and an integer c, find a subset of A which sums to c. In general, this problem is known to be NP-complete. However, if A is superincreasing, meaning that each element of the set is greater than the sum of all the numbers in the set lesser than it, the problem is \"easy\" and solvable in polynomial time with a simple greedy algorithm.\
    In Merkle-Hellman, decrypting a message requires solving an apparently \"hard\" knapsack problem. The private key contains a superincreasing list of numbers W, and the public key contains a non-superincreasing list of numbers B, which is actually a \"disguised\" version of W. The private key also contains some \"trapdoor\" information that can be used to transform a hard knapsack problem using B into an easy knapsack problem using W. "
]


def test_solitaire(message, deck):
    """Test solitaire encryption and decryption using a deck configuration.
        (encrypt the message, then decrypt it and compare it with the original)

    :param message: The message to be decrypted.
    :type message: string
    :param deck: The order of the deck of cards.
    :type deck: list of ints

    :returns: Logical value whether the test was successful.
    """

    # save the deck
    d = deck.copy()

    # encrypt
    enc_msg = encrypt_solitaire(message, deck)

    # decrypt
    dec_msg = decrypt_solitaire(enc_msg, d)

    # compare
    if (message == dec_msg):
        return True
    else:
        return False


# keep only letters in the message
msg_test = [''.join(filter(str.isalpha, msg)) for msg in msg_test]

deck = list(range(54))
correct = 0
for i in range(n_test):
    # shuffle deck randomly
    shuffle(deck)

    # test on all messages
    for message in msg_test:
        if (test_solitaire(message, deck)):
            correct += 1
        else:
            print("not correct", message)

# print test summary
print("Algorithm worked correctly for",
    int(correct / (n_test * len(msg_test)) * 100),
    "% of the test cases.")
