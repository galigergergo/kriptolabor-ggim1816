import socket
from json import dumps
from merkle_hellmann import encrypt_mh
from time import sleep
from clients import prepare_communication
from random import shuffle
from solitaire import encrypt_solitaire, decrypt_solitaire


# own host/ip address and port number
MYHOST = 'client1'
MYPORT = 65001

# other client host/ip address and port number
OTHERHOST = 'client2'
OTHERPORT = 65002

# message to be sent to other client
TOSEND = 'Both the sender and receiver share a single key. The sender uses this key to encrypt plaintext and send the cipher text to the receiver. On the other side the receiver applies the same key to decrypt the message and recover the plain text.'


# send a message to a socket address
def send(host, port, message):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        s.sendall(message.encode())


# get keys
my_pri_key, my_pub_key, other_pub_key = prepare_communication(MYPORT, OTHERPORT)

# generate random solitaire key
solitaire_key = list(range(54))
shuffle(solitaire_key)

sleep(1)

# send mh encrypted solitaire key to other client
send(OTHERHOST, OTHERPORT, dumps(encrypt_mh(dumps(solitaire_key), other_pub_key)))
print('Randomly generated solitarie deck encrypted with mh and sent:', solitaire_key)

# wait for message from other client
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((MYHOST, MYPORT))
    s.listen()

    message = ''
    while message != 'exit':
        # get and decrypt message from other client
        conn, addr = s.accept()
        message = conn.recv(4096).decode()
        message = decrypt_solitaire(message, solitaire_key)
        print('Message received:', message)

        # break if other client exited
        if message == 'exit':
            break

        # get user input message
        TOSEND = input('Send a message: ')

        # send an encrypted message using the common solitaire deck
        send(OTHERHOST, OTHERPORT, encrypt_solitaire(TOSEND, solitaire_key))
        print('Message encrypted with solitaire and sent:', TOSEND)

        # break if you exited
        if TOSEND == 'exit':
            break
