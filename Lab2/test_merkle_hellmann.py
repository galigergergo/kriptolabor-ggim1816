from merkle_hellmann import *

# key sizes to be tested
n_test = [4, 5, 6, 7, 8, 15, 64, 125]

# messages to be tested
msg_test = [
    "szia",
    "Ez egy mondat",
    "The Merkle-Hellman knapsack cryptosystem was one of the earliest public key cryptosystems. It was published by Ralph Merkle and Martin Hellman in 1978. A polynomial time attack was published by Adi Shamir in 1984. As a result, the cryptosystem is now considered insecure.",
    "Merkle-Hellman is a public key cryptosystem, meaning that two keys are used, a public key for encryption and a private key for decryption. It is based on the subset sum problem (a special case of the knapsack problem).[5] The problem is as follows: given a set of integers A and an integer c, find a subset of A which sums to c. In general, this problem is known to be NP-complete. However, if A is superincreasing, meaning that each element of the set is greater than the sum of all the numbers in the set lesser than it, the problem is \"easy\" and solvable in polynomial time with a simple greedy algorithm.\
    In Merkle-Hellman, decrypting a message requires solving an apparently \"hard\" knapsack problem. The private key contains a superincreasing list of numbers W, and the public key contains a non-superincreasing list of numbers B, which is actually a \"disguised\" version of W. The private key also contains some \"trapdoor\" information that can be used to transform a hard knapsack problem using B into an easy knapsack problem using W. "
]


def test_merkle_hellmann(message, n):
    """Test Merkle-Hellmann encryption and decryption using a deck configuration.
        (encrypt the message, then decrypt it and compare it with the original)

    :param message: The message to be decrypted.
    :type message: string
    :param n: The key size.
    :type n: int

    :returns: Logical value whether the test was successful.
    """

    # generate private key
    (w, q, r) = generate_private_key(n)

    # encode
    enc_msg = encrypt_mh(message, create_public_key((w, q, r)))

    # decode
    dec_msg = decrypt_mh(enc_msg, (w, q, r))

    # compare
    if (message == dec_msg):
        return True
    else:
        return False


correct = 0
for n in n_test:
    for message in msg_test:
        # test on all messages with all key sizes
        if (test_merkle_hellmann(message, n)):
            correct += 1
        else:
            print("not correct", n, message)

# print test summary
print("Algorithm worked correctly for",
    int(correct / (len(n_test) * len(msg_test)) * 100),
    "% of the test cases.")
