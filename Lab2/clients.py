import socket
from json import dumps, loads
from merkle_hellmann import generate_private_key, create_public_key
from time import sleep


# server host/ip address and port number
SERVERHOST = 'keyserver'
SERVERPORT = 65000


# send one request to the server and return the decoded response
def server_request(req):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((SERVERHOST, SERVERPORT))

        # send request
        s.sendall(req.encode())

        # return decoded response
        return s.recv(1024).decode()


# prepare for peer to peer communication with other clients
def prepare_communication(MYPORT, OTHERPORT):
    # generate merkle-hellmann keys
    my_pri_key = generate_private_key()
    my_pub_key = create_public_key(my_pri_key)
    print('Keys generated.')

    # register on key server
    reg_req = {
        "client_id": str(MYPORT),
        "public_key": dumps(my_pub_key)
    }
    resp = server_request(dumps(reg_req))
    if resp == 'updated':
        print('Registered successfully.')
    else:
        print('Unable to register.')
        return 'error'

    # get other clients public key from key server
    get_req = {
        "client_id": str(OTHERPORT),
    }
    other_pub_key = server_request(dumps(get_req))
    while other_pub_key == 'error':
        sleep(1)
        other_pub_key = server_request(dumps(get_req))
    print('Received other client public key.')

    return my_pri_key, my_pub_key, loads(other_pub_key)
